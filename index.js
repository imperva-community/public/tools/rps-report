const request = require("request-promise-native");
const fs = require("fs");
const jsonexport = require('jsonexport');
const execSync = require('child_process').execSync;

const ACCOUNT_ID = process.env.INCAPSULA_ACCOUNT_ID;
const API_ID = process.env.INCAPSULA_API_ID;
const API_KEY = process.env.INCAPSULA_API_KEY;
const AUTH_PARAMS = `api_id=${API_ID}&api_key=${API_KEY}`;

const URL = "https://my.incapsula.com";
const LIST_SITES = `${URL}/api/prov/v1/sites/list?${AUTH_PARAMS}&account_id=${ACCOUNT_ID}`;
const GET_SITE_STATUS = `${URL}/api/prov/v1/sites/status?${AUTH_PARAMS}&account_id=${ACCOUNT_ID}`;
const GET_SITE_STATS = `${URL}/api/stats/v1?${AUTH_PARAMS}`;

let results = [];
main();

/**
 * Iterates all sites in a given account and outputs a CSV and JSON file with the status of the SSL certificates for each domain.
 * If a custom certificate has been uploaded, the script will manually query the domain and pull the certificate creation dates.
 * @returns {Promise<void>}
 */
async function main() {
    if(!API_ID || !API_KEY || !ACCOUNT_ID) {
        console.log(`[FATAL] The environment variables INCAPSULA_ACCOUNT_ID, INCAPSULA_API_ID and INCAPSULA_API_KEY must be set.  Exiting...`)
        process.exit(1);
    }

    console.log(`[INFO] Collecting website data for account ${ACCOUNT_ID}`);
    let sites = [];
    let res = await getSites(0);
    sites = sites.concat(res);
    if(res.length === 100) {
        // Need to query for additional sites
        let i = 1;
        while(res.length === 100) {
            res = await getSites(i);
            sites = sites.concat(res);
            i++;
        }
    }

    for (let siteRes of sites) {
        let site = {
            "site_id": siteRes.site_id,
            "domain": siteRes.domain,
            "account_id": siteRes.account_id
        };

        try {
            let stats = await getSiteStatistics(site.site_id, "hits_timeseries,bandwidth_timeseries,caching_timeseries", false);
            site = calculateRps(stats, site);
            site = calculateBandwidth(stats, site);
            site = calculateBps(stats, site);
            site = calculateCachedBandwidth(stats, site);
        }
        catch(e) {
            console.log(`[ERROR] ${e}`);
        }
        
        results.push(site);
    }

    // Write the output to CSV
    console.log(`[INFO] Writing results to ./out/account-${ACCOUNT_ID}.json`);
    fs.writeFileSync(`./out/account-${ACCOUNT_ID}.json`, JSON.stringify(results, null, 3));

    // Write the output to JSON
    jsonexport(results, (err, csv) => {
        if(!err) {
            console.log(`[INFO] Writing results to ./out/account-${ACCOUNT_ID}.csv`);
            fs.writeFileSync(`./out/account-${ACCOUNT_ID}.csv`, csv);
        }
        else {
            console.log(`[ERROR] ${err.message}`);
        }

    });

}

/**
 * Queries the Imperva Cloud WAF API and returns an array of all sites.  The first 100 sites will be retrieved by the API by default.
 * Additional pages can be queried by passing in a positive integer for pageNumber.
 * @param pageNumber: number
 * @returns {Promise<Array<Object>>}
 */
async function getSites(pageNumber = 0, debug = false) {
    console.log(`[INFO] POST /api/prov/v1/sites/list?page_num=${pageNumber}`);
    try {
        let response = await request.post({url: `${LIST_SITES}&page_size=100&page_num=${pageNumber}`, json: true, body: ""});
        console.log(`[INFO] Cloud WAF Response Status: ${response.res}`);
        if(debug) {
            //fs.writeFileSync(`./out/account-${ACCOUNT_ID}-${pageNumber}-debug.json`, JSON.stringify(response, null, 3));
        }
        if(response.res === 0) {
            console.log(`[INFO] Successfully retrieved ${response.sites.length} sites`);
            return response.sites;
        }
        else {
            console.log(`[FATAL] Failed to fetch site data with error ${response.res_message}`);
            process.exit(1);
        }
    }
    catch (e) {
        console.log(`[FATAL] Failed to fetch site data with error ${e.message}`);
        process.exit(1);
    }
}

/**
 * Queries the Imperva Cloud WAF API and returns the specified site statistics
 * @param siteId
 * @param stats
 * @param debug
 * @returns {Promise<void>}
 */
async function getSiteStatistics(siteId, stats, debug = false) {
    console.log(`[INFO] POST /api/stats/v1?site_id=${siteId}`);
    try {
        let response = await request.post({url: `${GET_SITE_STATS}&site_id=${siteId}&stats=${stats}&time_range=last_90_days&granularity=86400000`, json: true, body: ""});
        console.log(`[INFO] Cloud WAF Response Status: ${response.res}`);
        if(debug) {
            fs.writeFileSync(`./site-${siteId}-debug.json`, JSON.stringify(response, null, 3));
        }
        if(response.res === 0) {
            console.log(`[INFO] Successfully retrieved stats for site ${siteId}`);
            return response;
        }
        else {
            console.log(`[FATAL] Failed to fetch site data with error ${response.res_message}`);
            process.exit(1);
        }
    }
    catch (e) {
        console.log(`[FATAL] Failed to fetch site data with error ${e.message}`);
        process.exit(1);
    }
}

/**
 * Calculates request per second (rps) for each site
 * @param stats: Stats
 * @param site: Site
 * @returns {site: Site}
 */
function calculateRps(stats, site) {
    // Calculate RPS
    let humanRPS = stats.hits_timeseries.find( timeseries => timeseries.id === "api.stats.hits_timeseries.human_ps" );
    let botRPS = stats.hits_timeseries.find( timeseries => timeseries.id === "api.stats.hits_timeseries.bot_ps" );
    let totalRPS = [];
    for(let i=0; i<90; i++) {
        if(humanRPS.data.length !== 0 && botRPS.data.length !== 0) {
            if(!humanRPS.data[i] || !botRPS.data[i]) {
                totalRPS.push(0);
            }
            else {
                totalRPS.push(humanRPS.data[i][1] + botRPS.data[i][1]);
            }
        }
        else {
            totalRPS.push(0);
        }
    }
    site["Max Load (rps)"] = Math.max(...totalRPS);
    site["Min Load (rps)"] = Math.min(...totalRPS);
    site["Avg Load (rps)"] = Math.ceil(totalRPS.reduce((prev, curr) => prev + curr) / totalRPS.length);
    return site;
}

/**
 * Calculates bandwidth (bytes) for each site
 * @param stats: Stats
 * @param site: Site
 * @returns {site: Site}
 */
function calculateBandwidth(stats, site) {
    // Calculate Bandwidth
    let bandwidth = stats.bandwidth_timeseries.find( timeseries => timeseries.id === "api.stats.bandwidth_timeseries.bandwidth" );
    let totalBandwidth = [];
    for(let i=0; i<90; i++) {
        if(bandwidth.data.length !== 0) {
            if(!bandwidth.data[i]) {
                totalBandwidth.push(0);
            }
            else {
                totalBandwidth.push(bandwidth.data[i][1]);
            }
        }
        else {
            totalBandwidth.push(0);
        }
    }
    site["Max Bandwidth (Bytes)"] = Math.max(...totalBandwidth);
    site["Min Bandwidth (Bytes)"] = Math.min(...totalBandwidth);
    site["Avg Bandwidth (Bytes)"] = Math.ceil(totalBandwidth.reduce((prev, curr) => prev + curr) / totalBandwidth.length);
    return site;
}

/**
 * Calculates throughput (bps) for each site
 * @param stats: Stats
 * @param site: Site
 * @returns {site: Site}
 */
function calculateBps(stats, site) {
    // Calculate bps
    let bps = stats.bandwidth_timeseries.find( timeseries => timeseries.id === "api.stats.bandwidth_timeseries.bps" );
    let totalBPS = [];
    for(let i=0; i<90; i++) {
        if(bps.data.length !== 0) {
            if(!bps.data[i]) {
                totalBPS.push(0);
            }
            else {
                totalBPS.push(bps.data[i][1]);
            }
        }
        else {
            totalBPS.push(0);
        }
    }
    site["Max Throughput (bps)"] = Math.max(...totalBPS);
    site["Min Throughput (bps)"] = Math.min(...totalBPS);
    site["Avg Throughput (bps)"] = Math.ceil(totalBPS.reduce((prev, curr) => prev + curr) / totalBPS.length);
    return site;
}

/**
 * Calculates total bandwidth cached for each site
 * @param stats: Stats
 * @param site: Site
 * @returns {site: Site}
 */
function calculateCachedBandwidth(stats, site) {
    // Calculate bps
    let cachedBandwidth = stats.caching_timeseries.find( timeseries => timeseries.id === "api.stats.caching_timeseries.bytes.total" );
    let totalCachedBandwidth = [];
    for(let i=0; i<90; i++) {
        if(cachedBandwidth.data.length !== 0) {
            if(!cachedBandwidth.data[i]) {
                totalCachedBandwidth.push(0);
            }
            else {
                totalCachedBandwidth.push(cachedBandwidth.data[i][1]);
            }
        }
        else {
            totalCachedBandwidth.push(0);
        }
    }
    site["Max Cached Bandwidth (Bytes)"] = Math.max(...totalCachedBandwidth);
    site["Min Cached Bandwidth (Bytes)"] = Math.min(...totalCachedBandwidth);
    site["Avg Cached Bandwidth (Bytes)"] = Math.ceil(totalCachedBandwidth.reduce((prev, curr) => prev + curr) / totalCachedBandwidth.length);
    site["Total Cached Bandwidth (Bytes)"] = totalCachedBandwidth.reduce((prev, curr) => prev + curr);
    return site;
}
