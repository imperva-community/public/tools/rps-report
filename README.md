# Imperva Cloud WAF RPS Report
 
Reporting script that aggregates Request Per Second for every website in a Cloud WAF account and sub-account.
Report details are output in .csv and .json formats and includes Max, Min, and Avg RPS for each site.

## Installation
Update your **environment variables** to specify your API ID, API Key, and account ID.

`INCAPSULA_ACCOUNT_ID` _(required)_   - the account ID associated with your Imperva Cloud WAF account  
`INCAPSULA_API_ID` _(required)_       - the API ID of a Cloud WAF user with READ permissions  
`INCAPSULA_API_KEY` _(required)_      - the API Key for the Cloud WAF user

Execute the following commands.
```
npm install
node index.js
```

## Example

```
$ node index.js 
[INFO] POST /api/stats/v1?site_id=100682885
[INFO] Cloud WAF Response Status: 0
[INFO] Successfully retrieved stats for site 123456789
[INFO] Writing results to account-12345.json
[INFO] Writing results to account-12345.csv
```
